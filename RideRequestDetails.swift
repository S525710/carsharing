//
//  RideRequestDetails.swift
//  CarSharing
//
//  Created by Ryan Carley on 11/5/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import Foundation
import Parse
import Bolts

class Request: PFObject, PFSubclassing {
    @NSManaged var starting:String!
    @NSManaged var destination:String!
    @NSManaged var seats:String!
    @NSManaged var time:String!
    
    
    static func parseClassName() -> String {
        return "Request"
    }
    
}
