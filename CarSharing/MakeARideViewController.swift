//
//  FirstViewController.swift
//  CarSharing
//
//  Created by Shobhit Dobhal on 10/6/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class MakeARideViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var startingPKR: UIPickerView!
    @IBOutlet weak var destinationPKR: UIPickerView!
    @IBOutlet weak var seatsTF: UITextField!
    @IBOutlet weak var timeDP: UIDatePicker!
    var date:String = ""
    
    @IBAction func getTime(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        let strDate = dateFormatter.stringFromDate(timeDP.date)
        date = strDate
    }
    
    @IBAction func makeRideBTN(sender: AnyObject) {
        let newRide = Request()
        
        newRide.starting = String(startingPKR.selectedRowInComponent(0))
        newRide.destination = String(destinationPKR.selectedRowInComponent(0))
        newRide.seats = seatsTF.text
        newRide.time = date
        
        displayMessage("Your ride has been successfully made!")
    }
  
  
    @IBOutlet weak var seatsAvailableTF: UITextField!
  
        var picker1Options = ["1","2"]
        var picker2Options = []
        var picker3Options = []
    
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
       override  func viewDidLoad() {
            super.viewDidLoad()
            picker1Options = ["NorthWest Missouri State University","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St"]
            picker2Options = ["NorthWest Missouri State University","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St"]
            
        
            // Uncomment the following line to preserve selection between presentations
            // self.clearsSelectionOnViewWillAppear = false
            
            // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
            // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        }
        
        
        func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if (pickerView.tag == 1){
                return picker1Options.count
            }else {
                return picker2Options.count
            }
            
        }
        
        func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
            if (pickerView.tag == 1){
                print(picker1Options[row].characters)
                return "\(picker1Options[row])"
            }else {
                return "\(picker2Options[row])"
            }
        }
    
    func numOfLocations() -> Int{
        return picker1Options.count
    }
        
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
}

