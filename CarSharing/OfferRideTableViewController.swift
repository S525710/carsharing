//
//  OfferRideTableViewController.swift
//  CarSharing
//
//  Created by Ryan Carley on 11/5/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class OfferRideTableViewController: UITableViewController {
    
    let NAME_TAG = 100
    let STARTING_TAG = 101
    let DESTINATION_TAG = 102
    let SEATS_TAG = 103
    let TIME_TAG = 104
    let number:MakeARideViewController = MakeARideViewController()
    var request:Request!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return number.numOfLocations()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("offer_ride", forIndexPath: indexPath)
        let nameLBL:UILabel = cell.viewWithTag(NAME_TAG) as! UILabel
        let startingLBL:UILabel = cell.viewWithTag(STARTING_TAG) as! UILabel
        let destinationLBL:UILabel = cell.viewWithTag(DESTINATION_TAG) as! UILabel
        let seatsLBL:UILabel = cell.viewWithTag(SEATS_TAG) as! UILabel
        let timeLBL:UILabel = cell.viewWithTag(TIME_TAG) as! UILabel
        let query = PFQuery(className:"Request")
        
        
        //nameLBL.text
        //startingLBL.text
        //destinationLBL.text
        //seatsLBL.text
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
