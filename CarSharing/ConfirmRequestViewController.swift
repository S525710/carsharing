//
//  ConfirmRequestViewController.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 10/25/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit

class ConfirmRequestViewController: UIViewController {

    @IBAction func ConfirmRequestBTN(sender: AnyObject) {
        displayMessage("Your Request has been successfully made")
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
