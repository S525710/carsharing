//
//  FlashImage.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 10/20/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import Foundation

import UIKit

class FlashImage: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        performSelector(#selector(FlashImage.ShowNavController), withObject: nil, afterDelay: 1.5)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func ShowNavController(){
        self.performSegueWithIdentifier("loginView", sender: self)
    }
    
}