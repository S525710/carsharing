//
//  RegistrationViewController.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 10/25/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class RegistrationViewController: UIViewController {

    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var number919TF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var rePasswordTF: UITextField!
  
    
    @IBAction func registerBTN(sender: AnyObject) {
        
        let lastName = self.lastNameTF.text
        let firstName = self.firstNameTF.text
        let username = self.number919TF.text
        let password = self.passwordTF.text
        
        
//        let finalEmail = email!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        let newUser = PFUser()
        let studentObject = StudentDetails()
        
       // newUser.username = "\(email![email!.startIndex...email!.startIndex.advancedBy(6)])"
        
        newUser.username = username
      
        newUser.password = password
        //newUser.email = finalEmail
        
        studentObject.firstName = firstNameTF.text!
        studentObject.lastName = lastNameTF.text!
        studentObject.username = number919TF.text!
        studentObject.password = passwordTF.text!
        
        
        newUser.signUpInBackgroundWithBlock({ (succeed, error) -> Void in
            
            if ((error) != nil) {
                let alert = UIAlertController(title: "Error", message:"\(error)", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
            } else {
                let alert = UIAlertController(title: "Success", message:"Signed up successfully!", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Login")
                    self.presentViewController(viewController, animated: true, completion: nil)
                })
            }
        })
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
