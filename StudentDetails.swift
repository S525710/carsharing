//
//  StudentDetails.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 11/3/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import Foundation
import Parse
import Bolts

class StudentDetails: PFObject, PFSubclassing {
    @NSManaged var firstName:String!
    @NSManaged var lastName:String!
    @NSManaged var username:String!
    @NSManaged var password:String!

    
    static func parseClassName() -> String {
        return "Student"
    }
    
}
