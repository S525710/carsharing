//
//  PostHistoryDetailsViewController.swift
//  CarSharing
//
//  Created by ios group on 11/7/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

//
//  LoginViewController.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 10/25/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class PostHistoryDetailsViewController: UIViewController,UITextFieldDelegate {
    
    var x = ""
    let testObject = PFObject(className: "offer_ride")
    
    
    @IBOutlet weak var datepicker: UIDatePicker!
    
    @IBOutlet weak var frompoint: UIPickerView!
    
    @IBOutlet weak var topoint: UIPickerView!
    
    
    
    @IBOutlet weak var seats: UIPickerView!
    
    
    
    @IBOutlet weak var editbtn: UIButton!
    
    
    
    
    @IBAction func datePickerAction(sender: AnyObject) {
        
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        var strDate = dateFormatter.stringFromDate(datepicker.date)
        print(strDate)
        testObject["date1"] = datepicker.date
    }
    
    
    
    @IBAction func editRideBTN(sender: AnyObject) {
        editbtn.setTitle("Save", forState: .Normal)
        
        datepicker.userInteractionEnabled = true
        frompoint.userInteractionEnabled = true
        topoint.userInteractionEnabled = true
        seats.userInteractionEnabled = true
        
        
        
        
        
        
        displayMessage("Your Ride is Successfully made and Available ")
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1 {
            testObject["fromlocation"] = picker1Options[row]
        } else if pickerView.tag == 2 {
            testObject["tolocation"] = picker2Options[row]
        } else if pickerView.tag == 3 {
            testObject["seats"] = picker3Options[row]
        }
        
        
        
        
        
        
        
    }
    
   
    
    @IBAction func logoutBTN(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
        
        //   self.performSegueWithIdentifier("logout", sender:sender)
        
    }
    
    
    @IBAction func historyBTN(sender: AnyObject) {
        
        
        
        
        //  self.performSegueWithIdentifier("history", sender:sender)
        
    }
    
    
    
    
    
    
    
    
    
    
    var picker1Options = ["1","2"]
    var picker2Options = []
    var picker3Options = []
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    override  func viewDidLoad() {
        super.viewDidLoad()
        picker1Options = ["NorthWest Missouri State University","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St"]
        picker2Options = ["NorthWest Missouri State University","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St"]
        picker3Options = [1,2,3,4,5,6,7]
        let todaysDate = NSDate()
        
        datepicker.minimumDate = todaysDate
        
        testObject["fromlocation"] = "Horizons West"
        testObject["tolocation"] = "Walnut St"
        testObject["date1"] = todaysDate
        testObject["seats"] = 3
        datepicker.userInteractionEnabled = false
        frompoint.userInteractionEnabled = false
        topoint.userInteractionEnabled = false
        seats.userInteractionEnabled = false
        //topoint.selectRow(0, inComponent: 0, animated: false)
        // datepicker.date = todaysDate
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView.tag == 1){
            return picker1Options.count
        }
        if (pickerView.tag == 2) {
            return picker2Options.count
        }
        else {
            print(picker3Options.count)
            return picker3Options.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if (pickerView.tag == 1){
            print(picker1Options[row].characters)
            return "\(picker1Options[row])"
        }
        if (pickerView.tag == 3){
            // print(picker3Options[row].characters)
            print("\(picker3Options[row])")
            return "\(picker3Options[row])"
        }
        else {
            return "\(picker2Options[row])"
        }
        
        
        
    }
    
    
    
    
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "all fields are not entered", message: message,
                                      
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    
}

