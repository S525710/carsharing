//
//  FirstViewController.swift
//  CarSharing
//
//  Created by Shobhit Dobhal on 10/6/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class FirstViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
   
    var x = ""
    let testObject = PFObject(className: "offer_ride")
    
    
    @IBOutlet weak var datepicker: UIDatePicker!
    
    @IBOutlet weak var frompoint: UIPickerView!
    
    @IBOutlet weak var topoint: UIPickerView!
    
    
    
    @IBOutlet weak var seats: UIPickerView!
    
    
    
    
    
    
    
    @IBAction func datePickerAction(sender: AnyObject) {
        
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        var strDate = dateFormatter.stringFromDate(datepicker.date)
        print(strDate)
        testObject["date1"] = strDate
    }
    
    
    
    @IBAction func makeRideBTN(sender: AnyObject) {
        
        
        
   //     testObject["fromlocation"] = frompoint.selectedRowInComponent(0)
    //    testObject["tolocation"] = "ddd"
      //  testObject["date"] = frompoint.selectedRowInComponent(0)
     //  testObject["seats"] = 4
      //  testObject["username"] = studentObject.username
     //   testObject["password"] = studentObject.password
        
        testObject.saveInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
            print("Object has been saved.")
            print("testing")
        }

        
        
        
        
        
    displayMessage("Your Ride is Successfully made and Available ")
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       
        if pickerView.tag == 1 {
            testObject["fromlocation"] = picker1Options[row]
        } else if pickerView.tag == 2 {
            testObject["tolocation"] = picker2Options[row]
        } else if pickerView.tag == 3 {
           testObject["seats"] = picker3Options[row]
        }
        
        
        
        
        
                
        
    }
  
    @IBAction func logoutBTN(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
     
        
     //   self.performSegueWithIdentifier("logout", sender:sender)
       
    }
    
    
    @IBAction func historyBTN(sender: AnyObject) {
        
        
        
        
   //  self.performSegueWithIdentifier("history", sender:sender)
        
    }


  

  
    
 
   
        
        
        var picker1Options = ["1","2"]
        var picker2Options = []
        var picker3Options = []
        
        
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
    
    
        
       override  func viewDidLoad() {
            super.viewDidLoad()
            picker1Options = ["NorthWest Missouri State University","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St"]
            picker2Options = ["NorthWest Missouri State University","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St"]
            picker3Options = ["1","2","3","4","5","6","7"]
        let todaysDate = NSDate()
       
        datepicker.minimumDate = todaysDate
       
        
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        var strDate = dateFormatter.stringFromDate(todaysDate)
        print(strDate)
        testObject["date1"] = strDate

        
        testObject["fromlocation"] = "Horizons West"
        testObject["tolocation"] = "Walnut St"
      //  testObject["date1"] = todaysDate
        testObject["seats"] = "1"
        
        //topoint.selectRow(0, inComponent: 0, animated: false)
       // datepicker.date = todaysDate
            // Uncomment the following line to preserve selection between presentations
            // self.clearsSelectionOnViewWillAppear = false
            
            // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
            // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        }
        
        
        func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
            return 1
        }
        
        func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if (pickerView.tag == 1){
                return picker1Options.count
            }
            if (pickerView.tag == 2) {
                return picker2Options.count
            }
            else {
                print(picker3Options.count)
                return picker3Options.count
            }
        }
        
        func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
            if (pickerView.tag == 1){
                print(picker1Options[row].characters)
                return "\(picker1Options[row])"
            }
            if (pickerView.tag == 3){
               // print(picker3Options[row].characters)
                print("\(picker3Options[row])")
                return "\(picker3Options[row])"
            }
            else {
                return "\(picker2Options[row])"
            }

            
            
        }
        
        

  
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "all fields are not entered", message: message,
                                      
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
}

