//
//  PostHistoryTableViewController.swift
//  CarSharing
//
//  Created by ios group on 11/5/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse

class PostHistoryTableViewController: UITableViewController {
    
    @IBOutlet var tableView1: UITableView!
    
    let NAME_TAG = 1
    let SEAT_TAG = 2
    var fromResults:[String] = []
    var toResults:[String] = []
    var seatsResults:[String] = []
    var dateResults:[String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let query = PFQuery(className: "offer_ride")
        query.findObjectsInBackgroundWithBlock { (objects, error) in
            
            if error == nil {
                
                if objects != nil {
                    
                    for object in objects! {
                        var x = object["fromlocation"] as! String
                       self.fromResults.append(x)
                       print(x)
                        x = object["tolocation"] as! String
                        self.toResults.append(x)
                        x = object["date1"] as! String
                        self.dateResults.append(x)
                        x = object["seats"] as! String
                        self.seatsResults.append(x)
                    }
                     self.tableView1.reloadData()
                    
                }
                
            }
            
            
        }
       
            self.tableView1.reloadData()
    
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    override func viewWillAppear(animated: Bool) {
        
        self.tableView1.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fromResults.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("rides1", forIndexPath:indexPath)
        
        let fromLBL = cell.viewWithTag(NAME_TAG) as! UILabel
        let toLBL = cell.viewWithTag(SEAT_TAG) as! UILabel
       let dateLBL = cell.viewWithTag(3) as! UILabel
        let seatLBL = cell.viewWithTag(4) as! UILabel
        
        fromLBL.text = fromResults[indexPath.row]
        toLBL.text = toResults[indexPath.row]
        dateLBL.text = dateResults[indexPath.row]
        seatLBL.text = seatsResults[indexPath.row]

        // let cell1:UILabel=cell.viewWithTag(100) as! UILabel
        // cell1.text=ridesResults[indexPath.row]
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
