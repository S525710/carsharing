//
//  SecondViewController.swift
//  CarSharing
//
//  Created by Shobhit Dobhal on 10/6/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{

   
    var picker1Options = []
    var picker2Options = []
   
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func logoutBTN1(sender: AnyObject) {
        
        
        
        
        self.performSegueWithIdentifier("logout1", sender:sender)
        
    }

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker1Options = ["NorthWest Missouri State University","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St"]
        picker2Options = ["NorthWest Missouri State University","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St"]
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView.tag == 1){
            return picker1Options.count
        }else{
            return picker2Options.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if (pickerView.tag == 1){
            return "\(picker1Options[row])"
        }else{
            return "\(picker2Options[row])"
        }
    }
    

    
    
 
}

