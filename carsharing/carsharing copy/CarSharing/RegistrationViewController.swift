//
//  RegistrationViewController.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 10/25/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class RegistrationViewController: UIViewController {

    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var number919TF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var rePasswordTF: UITextField!
    @IBOutlet weak var contactTF: UITextField!
    
    @IBAction func registerBTN(sender: AnyObject) {
        
        let lastName = self.lastNameTF.text
        let firstName = self.firstNameTF.text
        let username = self.number919TF.text
        let password = self.passwordTF.text
        let repassword = self.rePasswordTF.text
    
        if(lastName!.isEmpty || firstName!.isEmpty || username!.isEmpty || password!.isEmpty || repassword!.isEmpty){
            displayMyAlertMessage("All fields are required")
        }
        
        //Check if passwords match
        if(password != repassword)
        {
            // Display an alert message
            displayMyAlertMessage("Passwords do not match");
            return;
        }

        
        
        
        
        
        
        //        let finalEmail = email!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        let newUser = PFUser()
        
        let studentObject = StudentDetails()
        
       // newUser.username = "\(email![email!.startIndex...email!.startIndex.advancedBy(6)])"
        
        newUser.username = username
      
        newUser.password = password
      //  newUser.
        //newUser.email = finalEmail
        
        studentObject.firstName = firstNameTF.text!
        studentObject.lastName = lastNameTF.text!
        studentObject.username = number919TF.text!
        studentObject.password = passwordTF.text!
        
        let testObject = PFObject(className: "student")
        testObject["firstname"] = studentObject.firstName
        testObject["lastname"] = studentObject.lastName
        testObject["username"] = studentObject.username
        testObject["password"] = studentObject.password
        
        testObject.saveInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
            print("Object has been saved.")
            print("testing")
        }

        
        newUser.signUpInBackgroundWithBlock({ (succeed, error) -> Void in
            
            if ((error) != nil) {
                let alert = UIAlertController(title: "Error", message:"\(error)", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
            } else {
                var myAlert = UIAlertController(title:"Alert", message:"Registration is successful. Thank you!", preferredStyle: UIAlertControllerStyle.Alert);

                
                let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.Default){ action in
                    self.dismissViewControllerAnimated(true, completion:nil);
                }
                
                myAlert.addAction(okAction);
                self.presentViewController(myAlert, animated:true, completion:nil);
               // let alert = UIAlertController(title: "Success", message:"Signed up successfully!", preferredStyle: .Alert)
               // alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
             /*   self.presentViewController(alert, animated: true){}
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Home")
                    self.presentViewController(viewController, animated: true, completion: nil)
                }) */
            }
        })
        
    }
    
    func displayMyAlertMessage(userMessage:String)
    {
        
        var myAlert = UIAlertController(title:"Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.Alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.Default, handler:nil);
        
        myAlert.addAction(okAction);
        
        self.presentViewController(myAlert, animated:true, completion:nil);
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
