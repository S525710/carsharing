//
//  LoginViewController.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 10/25/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class LoginViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet weak var numberTF: UITextField!
    
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBAction func registerBTN(sender: AnyObject) {
      //  self.performSegueWithIdentifier("register", sender:sender)
    }

//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        //get a reference to the destination view controller
//        let destinationVC:FirstViewController = segue.destinationViewController as! FirstViewController
//        
//        //set properties on the destination view controller
//        destinationVC.x = numberTF.text!
//    }
    
    
    @IBAction func loginBTN(sender: AnyObject) {
        
        let username = self.numberTF.text
        let password = self.passwordTF.text
        
        if(username!.isEmpty || password!.isEmpty)
        {
            
            // Display alert message
            
            displayMyAlertMessage("All fields are required");
            
            return;
        }
        
        
        PFUser.logInWithUsernameInBackground(username!, password: password!, block: { (user, error) -> Void in
            
            
            
            if ((user) != nil) {
                let alert = UIAlertController(title: "Success", message:"Logged In!", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                                   self.presentViewController(alert, animated: true){}
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewControllerWithIdentifier("HomeNav")
                    self.presentViewController(viewController, animated: true, completion: nil)
                })
            } else {
                let alert = UIAlertController(title: "Error!", message:"Username and password do not match. Please check your credentials.", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                let viewController:UIViewController = UIStoryboard(name: "Main", bundle:nil).instantiateViewControllerWithIdentifier("HomeNav")
                self.presentViewController(viewController, animated: true, completion: nil)
                self.presentViewController(alert, animated: true){}
            }
        })
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {

        textField.resignFirstResponder()
        
        return true
    }
    
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    
    
    func displayAlertWithTitle(title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    func displayMyAlertMessage(userMessage:String)
    {
        
        var myAlert = UIAlertController(title:"Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.Alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.Default, handler:nil);
        
        myAlert.addAction(okAction);
        
        self.presentViewController(myAlert, animated:true, completion:nil);
        
    }
 
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberTF.delegate = self
        passwordTF.delegate = self
        let testObject = PFObject(className: "TestObject")
        testObject["foo"] = "bar"
        testObject.saveInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
            print("Object has been saved.")
            print("testing")
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
