//
//  Register.swift
//  CarSharing
//
//  Created by Ryan Carley on 11/2/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import Foundation
import Parse
import Bolts

class Register: PFObject, PFSubclassing{
    @NSManaged var  lastName:String
    @NSManaged var firstName:String
    @NSManaged var number:String
    @NSManaged var password:String
    
    static func parseClassName() -> String {
        return "Register"
    }
}
